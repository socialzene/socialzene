<img src="https://codeberg.org/avatars/56b70b6e7b3d01f7c29fecc5cbc99137737559fc9ebebd9cd0ca3592e3e9a1e0?size=280" width="200" height="200" />

> Notice: Socialzene is currently under development.

# Socialzene
> An upcoming social platform for everyone.

## Repositories

| Repository         | Description     | Maintainer |
|--------------|-----------|------------|
| [socialzene](https://codeberg.org/socialzene/readme) | project | [@rhylso](https://codeberg.org/rhylso)    |
| [client](https://codeberg.org/socialzene/client) | frontend | [@rhylso](https://codeberg.org/rhylso)       |
| [server](https://codeberg.org/socialzene/server) | backend | [@rhylso](https://codeberg.org/rhylso)        |

---
> Socialzene 2023.
